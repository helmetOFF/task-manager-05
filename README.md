## PROJECT INFO

TASK MANAGER

## DEVELOPER INFO

**NAME:** VLADISLAV HALMETOV

**E-MAIL:** halmetoff@gmail.com

## SOFTWARE

- JDK 1.8
- MS WINDOWS 10
- Apache Maven 3.6.3

## HARDWARE

**CPU:** Intel Core i5-4590 or better

**RAM:** 8 GB

**ROM:** 100 MB

## PROGRAM BUILD

```bash
mvn clean package
```

## PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

## SCREENSHOTS

https://drive.google.com/drive/folders/1lLAp0MHNXfelVE6Qg85wyD36H08mTbBr?usp=sharing